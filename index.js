import express from "express";
import { connectDb } from "./src/Connection/db.js";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import { API_PORT } from "./src/Helper/Constant.js";
import api from "./src/Routes/api.js";
import { fileURLToPath } from 'url';
import path, { dirname } from "path";
import cors from "cors";
dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = express();

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use("/file", express.static(path.join(__dirname, 'file')));
app.use(cors());

connectDb();

app.get("/", (req, res) => {
    res.status(200).json({
        name: "Default Route for Vascomm",
        version: "1.0.0",
    });
});
app.use("/api", api);

app.listen(API_PORT, () => {
    console.log(`Server Start at PORT ${API_PORT}`)
})