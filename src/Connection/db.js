import { connect } from 'mongoose';
import { MONGODB_URI } from '../Helper/Constant.js';
const uri = MONGODB_URI || '';
export const connectDb = async () => {
    await connect(uri)
        .then(() => {
            console.log(`MongoDB Running `, MONGODB_URI)
        })
        .catch((err) => {
            console.log(`MongoDB Error : ${err.message}`)
        })
}