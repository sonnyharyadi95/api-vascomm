import jsonwebtoken from "jsonwebtoken";
import { SECRET_KEY } from "../Helper/Constant.js";

export const generateToken = (data) => {
    return jsonwebtoken.sign(data, SECRET_KEY, {
        expiresIn: "1h",
    });
};

export const verifyToken = (req, res, next) => {
    const bearerHeader = req.header("Authorization");
    if (!bearerHeader) {
        res.status(401);
        return next("Unauthorized Access 1")
    }
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];

    if (!token) {
        res.status(401);
        return next("Unauthorized Access 2")
    }

    jsonwebtoken.verify(token, SECRET_KEY, (err, auth) => {
        if (err) {
            res.status(401);
            return next(err)
        }
    });
    req.body.token = token;

    next();
};
