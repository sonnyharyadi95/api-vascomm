import { Joi } from "express-validation";
import { PHOTO_PATTERN } from "../Helper/Constant.js";


export const UserValidation = {
    login: {
        body: Joi.object().required().keys({
            username: Joi.string(),
            password: Joi.string()
        })
    },
    register: {
        body: Joi.object().required().keys({
            name: Joi.string().exist(),
            username: Joi.string().exist(),
            password: Joi.string().exist(),
            email: Joi.string().email().exist(),
            photo: Joi.string().exist().regex(new RegExp(PHOTO_PATTERN)).messages({
                "string.pattern.base": "Not a Valid Image URL"
            }),
            type: Joi.string().exist()
        })
    }
}