import { User } from "../Model/User.js";
import pkg from 'bcryptjs';
const { compare, hash } = pkg;
import { ACCOUNT_CREATED, MAIL_EXIST, USER_PASS_INVALID } from "../Helper/Constant.js";
import { errorResponse, successResponse } from "../Helper/Response.js";
import { generateToken } from "../Middleware/jwt.js";
import jwt_decode from "jwt-decode";

export const Register = async (req, res) => {
    const { name, username, email, password, photo, type } = req.body;

    const userExist = await User.findOne({ email: email });
    if (userExist) {
        return errorResponse(res, MAIL_EXIST);
    }

    const dataUser = {
        name: name,
        username: username,
        email: email,
        password: await hash(password, 10),
        photo: photo,
        type: type
    }

    if (type != "Admin") {
        dataUser.approved = false;
    }

    const result = await new User(dataUser).save();
    return !result ? errorResponse(res, result) : successResponse(res, dataUser, ACCOUNT_CREATED);
}

export const Login = (req, res) => {
    const { username, password } = req.body;
    User.findOne({ username: username }).then((user) => {
        if (!user) {
            return errorResponse(res, USER_PASS_INVALID)
        }
        compare(password, user.password).then((same) => {
            if (!same) {
                return errorResponse(res, USER_PASS_INVALID);
            }

            const token = generateToken({ _id: user.id });
            return successResponse(res, { token: token });
        })
    })
}

export const ApproveCustomer = async (req, res) => {
    const token = req.body.token;
    const id = (jwt_decode(token))._id;
    const custId = req.params.id;

    const checkUserAdmin = checkUserLogin(id);
    if (!checkUserAdmin) {
        return errorResponse(res, 'OTORISASI');
    }

    const resultApprove = await User.findByIdAndUpdate(custId, { approved: true, approved_by: id, approved_at: new Date() });
    return !resultApprove ? errorResponse(res, resultApprove) : successResponse(res, null, 'APPROVED');
}

export const GetUserRegister = async (req, res) => {
    const token = req.body.token;
    const id = (jwt_decode(token))._id;

    const checkUserAdmin = checkUserLogin(id);
    if (!checkUserAdmin) {
        return errorResponse(res, 'OTORISASI');
    }

    const users = await User.find({ type: "Customer" });
    return !users ? errorResponse(res, 'No Data') : successResponse(res, users);
}

const checkUserLogin = async (userId) => {
    const checkUserAdmin = await User.findById(userId);
    return !checkUserAdmin ? false : checkUserAdmin.type == "Admin" ? true : false;
}