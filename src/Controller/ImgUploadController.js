import { DATA_NOT_FOUND, ERROR_INSERT, IMAGE_ONLY, IMAGE_ONLY_RESPONSE, IMAGE_UPLOADED } from "../Helper/Constant.js";
import { badRequestResponse, errorResponse, successResponse } from "../Helper/Response.js";
import { fileURLToPath } from 'url';
import path, { dirname } from "path";
import { ImageUpload } from "../Model/ImageUpload.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


export const UploadImage = (req, res) => {
    const file = req.files;
    if (!file) {
        return errorResponse(res, DATA_NOT_FOUND);
    }
    let imageFile = file.file;
    if ((imageFile.mimetype.split("/")[0]).toUpperCase() != IMAGE_ONLY) {
        return badRequestResponse(res, IMAGE_ONLY_RESPONSE);
    }

    imageFile.mv(path.join(__dirname + '../../../file/') + imageFile.name, (err) => {
        if (err) {
            return errorResponse(res, err)
        }

        let imageDataStore = new ImageUpload({
            name: imageFile.name,
            url: `${req.protocol}://${req.get('host')}/file/${imageFile.name}`,
            mimetype: imageFile.mimetype
        });

        imageDataStore.save().then((image) => {
            return !image ? errorResponse(res, ERROR_INSERT) : successResponse(res, image, IMAGE_UPLOADED)
        }).catch((e) => errorResponse(res, e))
    });
}