import { Schema, model } from 'mongoose';

const schema = new Schema({
    name: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true
    },
    mimetype: {
        type: String,
        required: true
    }
});

export const ImageUpload = model('ImageUpload', schema);