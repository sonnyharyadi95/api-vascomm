import { Schema, model } from 'mongoose';

const schema = new Schema({
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: true
    },
    approved: {
        type: Boolean,
        required: false
    },
    approved_by: {
        type: Schema.Types.ObjectId,
        required: false
    },
    approved_at: {
        type: Date,
        required: false
    }
},
    {
        timestamps: true
    });

export const User = model('User', schema);