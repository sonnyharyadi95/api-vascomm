import express from "express";
import userRoutes from "./user.js";
import imguploadRoutes from "./imgupload.js";
import { ValidationError } from "express-validation";
import * as responseHelper from "../Helper/Response.js";
import fileUpload from "express-fileupload";


const app = express();
app.use(fileUpload({
    createParentPath: true,
    limits: {
        fileSize: 1000000,
    },
    abortOnLimit: true
}));

app.get("/", (req, res) => {
    res.status(200).json({
        name: "Default Call to API",
    });
});

app.use("/user", userRoutes);
app.use("/upload", imguploadRoutes);

app.use((err, req, res, next) => {
    if (err instanceof ValidationError) {
        return responseHelper.validationResponse(res, err.details);
    }
    if (res.statusCode == 401) {
        return responseHelper.unauthorizedResponse(res, err)
    }
});

export default app;