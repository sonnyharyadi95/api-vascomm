import express from "express";
import { validate } from "express-validation";
import { UserValidation } from "../Middleware/validation.js";
import { Register, Login, GetUserRegister, ApproveCustomer } from "../Controller/UserController.js";
import { verifyToken } from "../Middleware/jwt.js";
const router = express.Router();

router.post('/register', validate(UserValidation.register), Register);
router.post('/login', validate(UserValidation.login), Login);
router.put('/approve-customer/:id', verifyToken, ApproveCustomer);
router.get('/', verifyToken, GetUserRegister);

export default router;