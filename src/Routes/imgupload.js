import express from "express";
import { verifyToken } from "../Middleware/jwt.js";
import { UploadImage } from "../Controller/ImgUploadController.js";

const router = express.Router();

router.post('/', verifyToken, UploadImage);

export default router;