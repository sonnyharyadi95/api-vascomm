const SUCCESS_MESSAGE = "Yay, success load data.";
const ERROR_MESSAGE = "Oops, failed load data.";
const VALIDATION_MESSAGE = "Oops, validation error.";

export const successResponse = (res, data, message) => {
    return res.status(200).json({
        status: true,
        message: message ? message : SUCCESS_MESSAGE,
        data: data,
    });
};

export const errorResponse = (res, message = ERROR_MESSAGE) => {
    return res.status(500).json({
        status: false,
        message: message,
        data: null,
    });
};

export const validationResponse = (
    res,
    data,
    message = VALIDATION_MESSAGE
) => {
    return res.status(400).json({
        status: false,
        message: message,
        data: data,
    });
};

export const notFoundResponse = (res, message) => {
    return res.status(404).json({
        status: false,
        message: message,
    });
};

export const unauthorizedResponse = (res, message) => {
    return res.status(401).json({
        status: false,
        message: message,
    });
};

export const badRequestResponse = (res, message) => {
    return res.status(400).json({
        status: false,
        message: message,
    });
}